# Репозиторий для курса по АКОСу у 151, 152, 053 групп. ###

## Полезные ссылки: 
1. [Ссылка](https://github.com/victor-yacovlev/fpmi-caos ) на репозиторий курса лектора 
2. [Ссылка](https://disk.yandex.ru/d/wkCK1LMzGNf4yA) на яндекс.диск с презентациями лекций: 
3.  [Ссылка](https://docs.google.com/spreadsheets/d/12DYcNNQuodssTZXN7R5pU59K0fXiB9V6qnEs-2QcUd0/edit#gid=0) на гугл-документ устных защит ассистентам

## Cеминары: 
1. [Введение, повторение инструментов разработки](https://gitlab.com/lushko.eseniya/diht-caos/-/blob/dev1/sem%201.md).
2. Командная строка линукс. 
